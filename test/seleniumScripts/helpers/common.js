// Put common functions here to be used in all tests

import SignUpPage from '../pageObjects/SignUpPage'
import uuidv1 from 'uuid/v1'
import faker from 'faker'
import SuggestPasswordPopUp from '../pageObjects/SuggestPasswordPopUp'
import TermsPopUp from '../pageObjects/TermsPopUp'
import PrivacyPopUp from '../pageObjects/PrivacyPopUp'

// Generates a number from 1 to n, default n = 20
const randomNumberGen = (n = 20) => (Math.random() * (n - 1) + 1) | 0

export const submitForm = () => {
  SignUpPage.open()
  //Checking if all properties of SignUpPageExist
  Object.getOwnPropertyNames(Object.getPrototypeOf(SignUpPage)).forEach(
    property => expect(SignUpPage[property]).to.exist
  )

  //  Starts filling the fields
  const companyName = uuidv1().replace(/-/g, '')
  SignUpPage.companyName.setValue(companyName)
  SignUpPage.countrySelector.click()
  SignUpPage.selectCountry(randomNumberGen()).click()
  SignUpPage.companyProfileSelector.click()
  SignUpPage.selectCompanyProfile(3).click()
  SignUpPage.numberOfEmployeesSelector.click()
  SignUpPage.selectNumberEmployees(3).click()
  SignUpPage.describeYourselfSelector.click()
  SignUpPage.selectDescription(3).click()
  expect(SignUpPage.inslyAddress.getValue()).to.equal(companyName)

  const adminWorkEmail = `${companyName}@putsbox.com`
  const fullName = `${faker.name.firstName()} ${faker.name.lastName()}`
  const number = '+5562882196860'
  SignUpPage.adminWorkEmail.setValue(adminWorkEmail)
  SignUpPage.accoutManagerName.setValue(fullName)

  // Password pop up about to open
  SignUpPage.suggestSecurePasswordLink.click()
  const password = SuggestPasswordPopUp.suggestedPassword.getText()
  SuggestPasswordPopUp.okBtn.click()

  SignUpPage.phone.setValue(number)

  expect(SignUpPage.adminWorkEmail.getValue()).to.equal(adminWorkEmail)
  expect(SignUpPage.accoutManagerName.getValue()).to.equal(fullName)
  expect(SignUpPage.phone.getValue()).to.equal(number)

  SignUpPage.termsCheckbox.click()
  SignUpPage.privacyCheckbox.click()
  SignUpPage.dataCheckbox.click()
  SignUpPage.termsLink.click()
  browser.pause(3000)
  TermsPopUp.agreeBtn.click()
  SignUpPage.privacyLink.click()
  browser.pause(3000)
  browser.execute(id => {
    document
      .getElementById(id)
      .scrollIntoView({ block: 'end', behavior: 'smooth' })
  }, 'document-content')
  browser.pause(1500)
  PrivacyPopUp.closeBtn.click()
  SignUpPage.submitBtn.waitForEnabled()
  expect(SignUpPage.submitBtn.isEnabled()).to.equal(true)

  SignUpPage.submitBtn.click()
  browser.pause(30000)

  return { adminWorkEmail, password, companyName }
}
