import SignInPage from '../../pageObjects/SignInPage'
import env from '../../../../env'
import { submitForm } from '../../helpers/common'

describe('Sign up tests', () => {
  it('Should sign up, log in and go to dashboard', () => {
    const { adminWorkEmail, companyName, password } = submitForm()

    const expectedDashboardUrl = `${env.baseUrl}/dashboard`.replace(
      'signup',
      companyName
    )

    SignInPage.SignInUserField.setValue(adminWorkEmail)
    SignInPage.SignInPasswordField.setValue(password)
    SignInPage.LogInBtn.click()
    expect(browser.getUrl()).to.equal(expectedDashboardUrl)
  })

  it('Should sign up and go directly to dashboard (logged in state)', () => {
    const { companyName } = submitForm()

    const expectedDashboardUrl = `${env.baseUrl}/dashboard`.replace(
      'signup',
      companyName
    )
    expect(browser.getUrl()).to.equal(expectedDashboardUrl)
  })
})
