import * as env from '../../../../env'

export default class Page {
  open(path = '') {
    browser.url(`${env.baseUrl}/${path}`)
  }
}
