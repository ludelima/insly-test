import Page from './baseClasses/Page'

class SignInPage extends Page {
  get SignInUserField() {
    return $('body #login_username')
  }

  get SignInPasswordField() {
    return $('body #login_password')
  }

  get SignInLoginLang() {
    return $('body #login_lang')
  }

  get LogInBtn() {
    return $('body #login_submit')
  }
}

export default new SignInPage()
